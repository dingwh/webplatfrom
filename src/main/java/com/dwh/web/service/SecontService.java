package com.dwh.web.service;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.dwh.web.mapper.CarInfoMapper;
import com.dwh.web.pojo.CarInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DS("slave_1")
public class SecontService {

    @Autowired
    CarInfoMapper carInfoMapper;

    public List<CarInfo> selectAll() {
        List<CarInfo> carInfos = carInfoMapper.selectAll();
        return carInfos;
    }

}
