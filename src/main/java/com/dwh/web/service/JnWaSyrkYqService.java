package com.dwh.web.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.dwh.web.mapper.JnWaSyrkYqMapper;
import com.dwh.web.pojo.JnWaSyrkYq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JnWaSyrkYqTmpRksj)表服务接口
 *
 * @author makejava
 * @since 2020-02-27 19:49:29
 */
@Service
@DS("slave_2")
public class JnWaSyrkYqService {

    @Autowired
    JnWaSyrkYqMapper jnWaSyrkYqMapper;

    public List<JnWaSyrkYq> selectBySfzh(String idCard){
        List<JnWaSyrkYq> jnWaSyrkYqs = jnWaSyrkYqMapper.selectBySfzh(idCard);
        return jnWaSyrkYqs;
    }

}