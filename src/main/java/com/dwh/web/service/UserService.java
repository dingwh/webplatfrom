package com.dwh.web.service;

import com.dwh.web.mapper.UserMapper;
import com.dwh.web.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    public List<User> selectUsers(int i) {
        List<User> users = userMapper.selectAll();
        return users;
    }

    public List<User> selectABC() {
        return userMapper.selectABC();
    }
}
