package com.dwh.web.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.dwh.web.mapper.JnWaSyfwYqRzxMapper;
import com.dwh.web.pojo.JnWaSyfwYq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (JnWaSyfwYqRzx)表服务接口
 *
 * @author makejava
 * @since 2020-02-27 19:45:43
 */
@Service
@DS("slave_2")
public class JnWaSyfwYqService {


    @Autowired
    JnWaSyfwYqRzxMapper jnWaSyfwYqRzxMapper;
    public List<JnWaSyfwYq> selectByOid(String oid) {
        return jnWaSyfwYqRzxMapper.selectByOid(oid);
    }
}