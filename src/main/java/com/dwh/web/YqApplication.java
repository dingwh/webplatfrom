package com.dwh.web;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.surfilter.exception.annotation.EnableExceptionHandling;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@EnableExceptionHandling
public class YqApplication {
    public static void main(String[] args) {
        SpringApplication.run(YqApplication.class, args);
    }

}
