package com.dwh.web.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class JnWaSyrkYq implements Serializable {
    private static final long serialVersionUID = -93567376239026319L;

    /*与房屋关联的ID*/
    private String oid;

    /*姓名*/
    private String xm;

    /*身份证号*/
    private String sfzh;

    /*联系方式*/
    private String lxfs;

    /*工作单位*/
    private String gzdw;

    /*当前情况（自住房，租赁房，其他）*/
    private String dqqk;

    /*地址是否一致（若是自住房，你的户籍地址与当前居住地址是否一致）*/
    private String dzsfyz;

    /*是否有以下症状(发热、咳嗽、呼吸困难、乏力、腹泻、没有以上症状)*/
    private String sfyzz;

    /*两周内是否接触（接触过、为接触）*/
    private String lznsfjc;

    /*所在小区是否有病例（有、没有）*/
    private String szxqsfybl;

    /*不一致地址（若不一致，您的户籍地址）*/
    private String byzdz;

    /*租赁房房东联系方式*/
    private String zlffdlxfs;

    /*租赁房房东姓名*/
    private String zlffdxm;

    /*问卷id*/
    private String answerId;

    /*创建时间（yyyy-MM-dd）*/
    private String createTimeYmd;

    /*入库时间（yyyy-MM-dd HH:mm:ss）*/
    private String rksj;

    /*证件类型（身份证、护照、港澳台通行证）*/
    private String zjlx;

    /*政治面貌（党员、群众、其他）*/
    private String zzmm;

    /*你志愿参加以下防控工作（网格长、楼长、单元长、志愿者、其他）*/
    private String nzycjyqfkgz;

    /*是否由外地返回（是，否）*/
    private String sfywdfh;

    /*外地返回出发地点*/
    private String wdfhcfdd;

    /*是否经停重点疫区*/
    private String sfjtzdyq;

    /*启程时间*/
    private String qcsj;

    /*到达时间*/
    private String ddsj;

    /*出行方式*/
    private String cxfs;

    /*请输入具体方式*/
    private String qsrjtfs;

    /*搭乘车牌号*/
    private String dccph;

    /*地址MD5编码*/
    private String dzmd5;

    /*ID*/
    private String flage;

    /*标记*/
    private String isHandle;


}