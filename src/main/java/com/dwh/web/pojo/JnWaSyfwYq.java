package com.dwh.web.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * (JnWaSyfwYqRzx)实体类
 *
 * @author makejava
 * @since 2020-02-27 19:45:39
 */
@Data
public class JnWaSyfwYq implements Serializable {
    private static final long serialVersionUID = 421195639711844842L;

    /*与人口关联的ID*/
    private String oid;

    /*调查员ID*/
    private String mid;

    /*小区、村庄*/
    private String village;

    /*门牌号*/
    private String location;

    /*几号楼*/
    private String building;

    /*创建时间 时间戳*/
    private String createTime;

    /*没用，全是0*/
    private String v;

    /*居委会/村委会/社区*/
    private String community;

    /*区*/
    private String district;

    /*街道办事处/镇*/
    private String street;

    /*创建时间（yyyy-MM-dd HH:mm:ss）*/
    private String createTimeStr;

    /*创建时间（yyyy-MM-dd）*/
    private String createTimeYmd;

    /*入库时间（yyyy-MM-dd HH:mm:ss）*/
    private String rksj;

    /*单元号*/
    private String unit;

    /*房间号*/
    private String room;

    /*ID*/
    private String id;

    /**/
    private String haozuo;

    /*区*/
    private String zfDistrict;

    /*标记*/
    private String isHandle;



}