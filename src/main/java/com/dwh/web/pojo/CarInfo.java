package com.dwh.web.pojo;

import lombok.Data;

/**
 * @Auther: fk
 * @Date: 2019/10/11 09:23
 * @Description:
 */

@Data
public class CarInfo {

    private String carnum;
    private String time;
}
