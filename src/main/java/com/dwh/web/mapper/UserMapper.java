package com.dwh.web.mapper;


import com.dwh.web.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface UserMapper  extends tk.mybatis.mapper.common.Mapper<User> {


    @Select("SELECT * FROM user where id > #{id}")
    List<User> selectUsers(@Param("id") Integer id);


    List<User> selectABC();




}
