package com.dwh.web.mapper;


import com.dwh.web.pojo.CarInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CarInfoMapper extends tk.mybatis.mapper.common.Mapper<CarInfo> {


    @Select("SELECT * FROM carinfo ")
    List<CarInfo> selectAll();

}
