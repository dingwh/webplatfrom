package com.dwh.web.mapper;

import com.dwh.web.pojo.JnWaSyfwYq;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * (JnWaSyfwYqRzx)表数据库访问层
 *
 * @author makejava
 * @since 2020-02-27 19:45:40
 */

@Mapper
@Resource
public interface JnWaSyfwYqRzxMapper {

    List<JnWaSyfwYq> selectByOid(String oid);
}