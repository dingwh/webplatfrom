package com.dwh.web.mapper;

import com.dwh.web.pojo.JnWaSyrkYq;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * (JnWaSyrkYq)表数据库访问层
 *
 * @author makejava
 * @since 2020-02-27 19:49:29
 */
@Mapper
@Resource
public interface JnWaSyrkYqMapper {

        List<JnWaSyrkYq> selectBySfzh(String idCard);

}