#!/bin/bash

# 
# This script for docker.
#

cd `dirname $0`
HOME=`pwd`
cd ..

DEPLOY_DIR=`pwd`

STDOUT_FILE=$DEPLOY_DIR/stdout.log
MAINWAR=$HOME/bootstrap.jar
MODEL=-Dspring.profiles.active=prod
MODEL="$MODEL -Dloader.path=lib,config"
MODEL="$MODEL -Dapplication.name=$DEPLOY_DIR"

JAVA_OPTS="-Xms3g -Xmx3g"
JAVA_OPTS="$JAVA_OPTS -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly -XX:+HeapDumpOnOutOfMemoryError"

echo java -jar $JAVA_OPTS $MODEL $MAINWAR \> $STDOUT_FILE 2\>\&1
echo OK!
java -jar $JAVA_OPTS $MODEL $MAINWAR > $STDOUT_FILE 2>&1